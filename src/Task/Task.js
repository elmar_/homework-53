import "./Task.css";


const Task = props => {
    return(
      <div className="Task">
          <p className="text">{props.text}</p>
          <button type="button" className="clean-btn" onClick={props.remove}>Clean</button>
      </div>
    );

};

export default Task;