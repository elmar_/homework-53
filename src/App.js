import React, {useState} from 'react';
import './App.css';
import AddTaskForm from './AddTaskForm/AddTaskForm';
import Task from './Task/Task';

const App = () => {

    const [tasks, setTasks] = useState([
        {text: 'DO homework', id: 1},
        {text: 'Clean home', id: 2}
    ]);

    let maxId = 0;
    for (let i = 0; i < tasks.length; i++) {
        if (tasks[i].id > maxId)
            maxId = tasks[i].id;
    }
    let newTask;

    const changeTask = (event) => {
            newTask = event.target.value;
    };

    const addTask = () => {
        maxId++;
        const taskCopy = {text: newTask, id: maxId};

        const tasksCopy = [...tasks];
        tasksCopy.push(taskCopy);
        setTasks(tasksCopy);
    }

    const removeTask = id => {
        const index = tasks.findIndex(task => id === task.id);
        const tasksCopy = [...tasks];
        tasksCopy.splice(index, 1);
        setTasks(tasksCopy);
    }

    let tasksList = (
      <>
          {
              tasks.map(task => {
                return <Task text={task.text} key={task.id} remove={() => removeTask(task.id)}/>
              })
          }
      </>
    );


  return (
    <div className="App">
      <AddTaskForm
          onTaskChange={event => changeTask(event)}
          onBtnClick={addTask}
      />
      {tasksList}
    </div>
  );
};

export default App;
