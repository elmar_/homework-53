// import React from 'react';
import './AddTaskForm.css';

const AddTaskForm = props => {
    return(
        <div className="AddTaskForm">
            <input type="text" className="input" onChange={props.onTaskChange}/>
            <button type="button" className="btn" onClick={props.onBtnClick}>Add</button>
        </div>
    )
}
export default AddTaskForm;